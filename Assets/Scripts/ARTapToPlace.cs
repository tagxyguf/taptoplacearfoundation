﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class ARTapToPlace : MonoBehaviour
{
    public GameObject objectToPlace;

    private GameObject spawnedObject;
    private ARRaycastManager arRaycastManager;
    private Vector2 touchPosition;

    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    private void Awake()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;
    }

    void Update()
    {
        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;

        if (arRaycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
        {
            var hitPose = hits[0].pose;

            if (spawnedObject == null) //instantiate if no object was instantiated
            {
                var newPosition = new Vector3(hitPose.position.x, hitPose.position.y + 0.05f, hitPose.position.z);
                var newRotation = hitPose.rotation * Quaternion.Euler(0, 180f, 0);
                spawnedObject = Instantiate(objectToPlace, newPosition, newRotation);
            }
            else //replace to new location if object was already instantiated
            {
                var newPosition = new Vector3(hitPose.position.x, hitPose.position.y + 0.05f, hitPose.position.z);
                spawnedObject.transform.position = newPosition;
            }
        }
    }
}