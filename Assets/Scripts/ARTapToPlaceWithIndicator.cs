﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class ARTapToPlaceWithIndicator : MonoBehaviour
{
    public GameObject placementIndicator;
    public GameObject objectToPlace;

    private GameObject spawnedIndicator;
    private GameObject spawnedObject;
    private ARRaycastManager arRaycastManager;
    private Vector2 touchPosition;
    private Pose placementPose;

    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
    
    private void Awake()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
    }

    bool TryGetTouchPosition(out Vector2 touchPosition) 
    {
        if(Input.touchCount > 0) 
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;
    }

    void Update()
    {
        //indicator placement
        var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var placeHits = new List<ARRaycastHit>();
        if(arRaycastManager.Raycast(screenCenter, placeHits, TrackableType.Planes))
        {
            if (spawnedIndicator == null) //instantiate if no object was instantiated
            {
                spawnedIndicator = Instantiate(placementIndicator);
            }
            
            if (placeHits.Count>0)
            {
                spawnedIndicator.SetActive(true);
                placementPose = placeHits[0].pose;
                var camForward = Camera.current.transform.forward;
                var camBearing = new Vector3(camForward.x, 0, camForward.z).normalized;
                placementPose.rotation = Quaternion.LookRotation(camBearing);
                spawnedIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);

                //touch and cube placement
                if (TryGetTouchPosition(out Vector2 touchPosition))
                {                    
                    if (spawnedObject == null) //instantiate if no object was instantiated
                    {
                        var newPosition = new Vector3(placementPose.position.x, placementPose.position.y + 0.05f, placementPose.position.z);
                        var newRotation = placementPose.rotation * Quaternion.Euler(0, 180f, 0);
                        spawnedObject = Instantiate(objectToPlace, newPosition, newRotation);
                    }
                    else //replace to new location if object was already instantiated
                    {
                        var newPosition = new Vector3(placementPose.position.x, placementPose.position.y + 0.05f, placementPose.position.z);
                        spawnedObject.transform.position = newPosition;
                    }
                }
            }
            else
            {
                spawnedIndicator.SetActive(false);
            }            
        }
        else
        {
            spawnedIndicator.SetActive(false);
        }
    }
}
