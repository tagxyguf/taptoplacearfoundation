# AR Project simple plane recognition and cube placement 

AR Project is a simple example with the ARFoundation setup for iOS and Android. It supports two simple features:
1) simple place recognition
2) simple spawning and placement of a cube

The result should look somehow like this on an Android Pixel 3:

![simple plane recognition](images/planedetection.png)
![simple cube placement](images/cubeplacement.png)
![simple cube placement](images/placementindicator.png)


## Project setup
Unity Version: 2022.3.47f1
Checkout the project and make sure to install the following packages using Window->PackagManager:
1) Apple ARKit XR Plugin - 5.1.5
2) Google ARCore XR Plugin - 5.1.5
3) ARFoundation - 5.1.5
Important: Please be aware to use the ARTap Scene and not the SampleScene. The ARTapScene is located in the root of the Asset folder.
Update: All packages are using updated versions now, before this worked with the preview versions.

### iOS (validated to run on iPhone 16 pro)
Make sure to have the iOS environment with xCode and setup stuff installed.
For iOS you need to have a Mac computer and create a xCode project from unity. 
Afterwards build and deploy from xCode to your iPhone. 

### Android (new updates not yet validated to run on Android phones)
Make sure to have the Android SDK and environment installed correctly and all paths set. 
You can directly build and deploy your unity application to your Android phone (make sure you have a AR enabled phone). 
If you have problems connecting to your Android phone you can use adb located in your Android SDK installation folder under platform-tools.

```bash
adb install pathtoyourapkandfile.apk
```

Sometimes your Android phone cannot be found, especially under MacOS. Then you can use adb usb to reset the usb connection.

```bash
adb usb
```

## Contributing
It is not planned to have contributions here.

## License
[MIT](https://choosealicense.com/licenses/mit/)
